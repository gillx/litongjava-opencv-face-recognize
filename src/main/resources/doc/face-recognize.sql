/*
SQLyog Ultimate v8.32 
MySQL - 5.5.62-log : Database - face_recognize
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`face_recognize` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `face_recognize`;

/*Table structure for table `employee` */

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` int(11) unsigned DEFAULT NULL COMMENT '员工组id',
  `department_id` int(11) unsigned DEFAULT NULL COMMENT '部门id',
  `type_id` int(11) unsigned DEFAULT NULL COMMENT '类型id',
  `photo_ids` varchar(255) DEFAULT NULL COMMENT '人脸图片id',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `gender` int(1) DEFAULT NULL COMMENT '性别,0: 未选择, 1: 男, 2: 女',
  `job_number` varchar(255) DEFAULT NULL COMMENT '工号',
  `title` varchar(255) DEFAULT NULL COMMENT '职务',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  `is_del` int(1) unsigned DEFAULT '0' COMMENT '是否删除,1删除,0未删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='员工';

/*Data for the table `employee` */

/*Table structure for table `employee_department` */

DROP TABLE IF EXISTS `employee_department`;

CREATE TABLE `employee_department` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '部门名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '部门描述',
  `is_del` int(1) unsigned DEFAULT '0' COMMENT '是否删除,1删除,0未删除',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='员工部门';

/*Data for the table `employee_department` */

insert  into `employee_department`(`id`,`name`,`desc`,`is_del`,`delete_time`,`create_time`,`update_time`) values (1,'技术部','技术部',0,NULL,'2020-09-28 15:20:38','2020-09-28 15:20:38'),(2,'行政部','行政部',0,NULL,'2020-09-28 15:20:49','2020-09-28 15:20:49');

/*Table structure for table `employee_group` */

DROP TABLE IF EXISTS `employee_group`;

CREATE TABLE `employee_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '分类描述',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `is_del` char(1) DEFAULT '0' COMMENT '是否删除,1删除,0未删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='员工分类';

/*Data for the table `employee_group` */

insert  into `employee_group`(`id`,`name`,`desc`,`delete_time`,`create_time`,`update_time`,`is_del`) values (1,'开发组','开发组',NULL,'2020-09-28 11:27:15','2020-09-28 15:21:12','0'),(2,'运维组','运维组',NULL,'2020-09-28 15:21:21','2020-09-28 15:21:21','0');

/*Table structure for table `employee_photo_face` */

DROP TABLE IF EXISTS `employee_photo_face`;

CREATE TABLE `employee_photo_face` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `md5` varchar(255) NOT NULL COMMENT '图片md5值',
  `uri` varchar(255) NOT NULL COMMENT '图片路径',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `is_del` int(1) unsigned DEFAULT '0' COMMENT '是否删除,1删除,0未删除',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `md5` (`md5`),
  UNIQUE KEY `uri` (`uri`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='员工人脸图片';

/*Data for the table `employee_photo_face` */

/*Table structure for table `employee_type` */

DROP TABLE IF EXISTS `employee_type`;

CREATE TABLE `employee_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '类型名称',
  `desc` varchar(255) DEFAULT NULL COMMENT '类型描述',
  `is_del` int(1) unsigned DEFAULT '0' COMMENT '是否删除,1删除,0未删除',
  `delete_time` datetime DEFAULT NULL COMMENT '软删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='员工类型';

/*Data for the table `employee_type` */

insert  into `employee_type`(`id`,`name`,`desc`,`is_del`,`delete_time`,`create_time`,`update_time`) values (1,'普通员工',NULL,0,NULL,NULL,NULL),(2,'访客',NULL,0,NULL,NULL,NULL),(3,'vip访客',NULL,0,NULL,NULL,NULL),(4,'经理',NULL,0,NULL,'2020-09-29 12:05:09','2020-09-29 12:05:09');

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `number` int(11) unsigned DEFAULT NULL COMMENT '编号',
  `login_name` varchar(100) DEFAULT NULL COMMENT '登录名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(100) DEFAULT NULL COMMENT '昵称',
  `register_ip` varchar(255) DEFAULT NULL COMMENT '注册ip',
  `is_del` char(1) DEFAULT '0' COMMENT '是否删除,1删除,0未删除',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`number`,`login_name`,`password`,`nick_name`,`register_ip`,`is_del`,`create_by`,`create_time`,`update_by`,`update_time`,`remarks`) values ('afce722c6dd849ab97bc59b4b43bc1bf',1,'admin','c7540eb7e65b553ec1ba6b20de79608',NULL,NULL,'0',NULL,'2020-08-29 21:59:07',NULL,'2020-08-29 21:59:07',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
