package com.litong.jfinal.druid;

import java.sql.SQLException;

import com.alibaba.druid.filter.FilterAdapter;
import com.alibaba.druid.filter.FilterChain;
import com.alibaba.druid.proxy.jdbc.StatementProxy;
import com.alibaba.druid.sql.SQLUtils;
import com.mysql.jdbc.PreparedStatement;

import lombok.extern.slf4j.Slf4j;

/**
 * 打印可执行sql语句 基于druid
 * @author bill robot
 * @date 2020年9月6日 下午9:36:57
 * @desc
 */
@Slf4j
public class SqlLogFilter extends FilterAdapter {
  private static final SQLUtils.FormatOption FORMAT_OPTION = new SQLUtils.FormatOption(false, false);

  @Override
  public void statement_close(FilterChain chain, StatementProxy statement) throws SQLException {
    if (statement.getRawObject() instanceof PreparedStatement) {
      PreparedStatement preparedStatement = (PreparedStatement) statement.getRawObject();
      String sql = preparedStatement.asSql();
      // 定制sql输出格式,可以去掉
      sql = SQLUtils.formatMySql(sql, FORMAT_OPTION);
      log.info("sql:{}", sql);
    }
    super.statement_close(chain, statement);
  }
}
