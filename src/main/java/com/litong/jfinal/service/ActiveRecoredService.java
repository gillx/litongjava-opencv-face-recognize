package com.litong.jfinal.service;

import java.util.List;

import com.jfinal.aop.Aop;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Table;
import com.jfinal.plugin.activerecord.TableMapping;

/**
 * @author litong
 * @date 2020年9月29日_上午8:14:51 
 * @version 1.0 
 * @param <M>
 * @desc
 */
public class ActiveRecoredService {

// 第一版
//  public EmployeePhotoFace findFirst(String select, String where, Class<EmployeePhotoFace> class1) {
//    return null;
//  }

// 第二版
//  public <T>T findFirst(String select, String where, Class<T> class1) {
//    return null;
//  }
// 第三版
//  public <T> T findFirst(String select, String where, Class<? extends Model> class1) {
//    return null;
//  }
  // 第 四 版
  public <M extends Model<M>> M findFirst(String select, String where, Class<M> modelClass, Object... params) {
    Table table = TableMapping.me().getTable(modelClass);
    M m = Aop.get(modelClass);
    if(StrKit.isBlank(where)) {
      return m.findFirst(select + " " + "from " + table.getName());
    }else {
      return m.findFirst(select + " " + "from " + table.getName() + " " + where, params);
    }
    
  }

  public <M extends Model<M>> List<M> find(String select, String where, Class<M> modelClass, Object... params) {
    Table table = TableMapping.me().getTable(modelClass);
    M m = Aop.get(modelClass);
    if(StrKit.isBlank(where)) {
      return m.find(select + " " + "from " + table.getName());
    }else {
      return m.find(select + " " + "from " + table.getName() + " " + where, params);
    }
  }

//  public static void main(String[] args) {
//    ActiveRecoredService activeRecoredService = Aop.get(ActiveRecoredService.class);
//    EmployeePhotoFace e = activeRecoredService.findFirst("select ", "where id=1", EmployeePhotoFace.class);
//  }
}
