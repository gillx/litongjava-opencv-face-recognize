package com.litong.jfinal.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.litong.jfinal.constants.TableConstants;
import com.litong.utils.digest.MD5Util;
import com.litong.utils.digest.SHA1Util;
import com.litong.utils.string.UUIDUtils;
import com.litong.utils.vo.JsonBean;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bill robot
 * @date 2020年8月29日_下午5:25:26 
 * @version 1.0 
 * @desc
 */
@Slf4j
public class UserService {

  /**
   * 添加用户
   * @param name
   * @param encodeText
   */
  public boolean add(String loginName, String encodeText) {
    String sql = "SELECT MAX(number) FROM "+TableConstants.SYS_USER;;
    Record record = Db.findFirst(sql);
    Long max = record.getLong("MAX(number)");
    if (max == null) {
      max = 1L;
    } else {
      max += 1;
    }
    Record sysUser = new Record();
    sysUser.set("id", UUIDUtils.random());
    sysUser.set("number",(Long) max);
    sysUser.set("loginName",loginName);
    sysUser.set("password",encodeText);
    sysUser.set("create_time",new Date());
    sysUser.set("update_time",new Date());
    return Db.save("sys_user", record);
  }

  /**
   * 登录用户
   * @param loginName
   * @param encodeText
   */
  public Record login(String loginName, String encodeText) {
    String sql = "select * from "+TableConstants.SYS_USER+" where login_name=? and password=?";
    Record sysUser = Db.findFirst(sql, loginName, encodeText);
    return sysUser;
  }

  /**
   * 返回角色
   * @param uid
   * @return
   */
  public List<JSONObject> getRelationRole(String uid) {
    List<JSONObject> data = new ArrayList<>();
    JSONObject j1 = new JSONObject();
    j1.put("roles_id", "1,2");
    data.add(j1);

    JSONObject j2 = new JSONObject();
    j2.put("roles_id", "3,4");
    data.add(j2);

    return data;
  }

  /**
   * 返回权限
   * @param role
   * @return
   */
  public List<JSONObject> getPermission(String role) {
    List<JSONObject> data = new ArrayList<>();
    JSONObject j1 = new JSONObject();
    j1.put("permissions_code", "1,2");
    data.add(j1);

    JSONObject j2 = new JSONObject();
    j2.put("permissions_code", "3,4");
    data.add(j2);
    return data;
  }

  public JsonBean<Void> updatePswd(String number, String oldPassowrd,String plainText) {
    //验证旧密码是否正确
    String odlPasswordencodeText = MD5Util.encode(SHA1Util.encode(oldPassowrd));
    String sql="select count(*) from sys_user where number=? and password=?";
    Record findFirst = Db.findFirst(sql, number,odlPasswordencodeText);
    Integer countUser = findFirst.getInt("count(*)");
    if(countUser<1) {
      return new JsonBean<Void>(-1, "密码不正确");
    }
      
    //对新密码进行加密
    log.info("update password the new password:{}",plainText);
    String encodeText = MD5Util.encode(SHA1Util.encode(plainText));
    //
    sql = "update sys_user set password=? where number=?";
    //log.info("{},{}",number,encodeText);
    int update = Db.update(sql, encodeText,number);
    log.info("update sys_user password result:{}",update);
    return new JsonBean<Void>("密码修改成功");
  }
}
