package com.litong.jfinal.controler;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.litong.jfinal.render.MimeTypeRender;
import com.litong.jfinal.validate.ImageValidator;

@Before(ImageValidator.class)
public class ImageController extends Controller {
  /**
   * filename可以包含路径,也可以不用包含路径
   * @param filename
   */
  public void index(String filename) {
    MimeTypeRender mimeTypeRender = new MimeTypeRender("jpg", filename);
    render(mimeTypeRender);
  }
}