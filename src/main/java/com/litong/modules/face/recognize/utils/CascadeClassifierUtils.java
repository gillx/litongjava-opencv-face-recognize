package com.litong.modules.face.recognize.utils;

import java.io.File;

import org.opencv.objdetect.CascadeClassifier;

import com.jfinal.kit.PathKit;
import com.litong.modules.face.recognize.constants.OpenCVConstats;
import com.litong.utils.file.PathUtils;

/**
 * @author bill robot
 * @date 2020年9月6日_下午8:06:44 
 * @version 1.0 
 * @desc
 */
public class CascadeClassifierUtils {
  private static String frontalfaceAltXml = null;
  private static String eyeXml = null;
  private static CascadeClassifier faceDetector = null;
  private static CascadeClassifier eyeDetector = null;

  static {
    // 创建2个人脸检测模型,faceDetector和eyeDetector,检测人脸和眼睛
    String openCVDir = PathKit.getRootClassPath() + File.separator + OpenCVConstats.openCVFolder;
    StringBuffer path = PathUtils.joint(openCVDir, "sources", "data", "haarcascades");
    frontalfaceAltXml = path.toString() + "haarcascade_frontalface_alt.xml";
    eyeXml = path.toString() + "haarcascade_eye.xml";
    faceDetector = new CascadeClassifier(frontalfaceAltXml);
    eyeDetector = new CascadeClassifier(eyeXml);
  }

  public static CascadeClassifier getFaceClassifier() {
    return faceDetector;
  }

  public static CascadeClassifier getEyeClassifier() {
    return eyeDetector;
  }

  public static String getFrontalfaceAltXml() {
    return frontalfaceAltXml;
  }

  public static String getEyeXml() {
    return eyeXml;
  }

}
